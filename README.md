# Projeto To School

1. **Link de acesso para o front:**
https://projetoescola-front.herokuapp.com

2. **Link de acesso para Api:**
https://projetoescola-api.herokuapp.com

3. **Documentação do Postman para executar as requisições para o back-end:**
https://documenter.getpostman.com/view/7160714/SVSBurid

4. No postman, funcionam todas requisições **exceto as pastas (Ministra, Avaliações
e Frequência)**.

5. O sistema ainda não conta com autenticação e nem é necessário login, basta ace
ssar o link e testar.


