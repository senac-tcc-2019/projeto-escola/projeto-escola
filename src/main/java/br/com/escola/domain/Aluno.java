package br.com.escola.domain;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Date;
import java.util.List;

@Entity
public @Data class Aluno {

    @ManyToMany(targetEntity = Avaliacao.class)
    private List<Avaliacao> avaliacao;

    @ManyToOne
    private Turma turma;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull
    private String nome;

    @NotNull
    private String cpf;

    @NotNull
    private Date data_nascimento;

   private boolean ativo;


}
