package br.com.escola.domain;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
public @Data class Avaliacao {

    @ManyToOne
    private Diciplina diciplina;

    @ManyToOne
    private Turma turma;

    @ManyToOne
    private Professor professor;


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull
    private String descricao;

    @NotNull
    private Date data;
}
