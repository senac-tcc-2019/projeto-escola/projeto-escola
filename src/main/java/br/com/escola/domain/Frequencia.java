package br.com.escola.domain;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
public @Data class Frequencia {

    @ManyToOne
    private Aluno aluno;

    @ManyToOne
    private Diciplina diciplina;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull
    private Date dia;

    private char presente;

}
