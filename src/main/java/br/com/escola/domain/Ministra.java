package br.com.escola.domain;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
public @Data class Ministra {

    @ManyToOne
    private Professor professor;

    @ManyToOne
    private Turma turma;

    @ManyToOne
    private Diciplina diciplina;



    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull
    private String ano_letivo;

}
