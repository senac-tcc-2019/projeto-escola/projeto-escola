package br.com.escola.domain;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Entity
public @Data class Professor {

    @OneToOne
    private Usuario usuario;


    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull
    private String nome;

    @NotNull
    private String cpf;

    @NotNull
    private Date data_nascimento;

    private Boolean ativo;



}
