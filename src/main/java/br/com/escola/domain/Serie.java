package br.com.escola.domain;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
public @Data class Serie {

    @ManyToOne
    private CategoriaEnsino categoriaEnsino;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull
    private int serie;

}
