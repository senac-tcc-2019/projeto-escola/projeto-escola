package br.com.escola.domain;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Entity
public @Data class Turma {

    @ManyToMany(targetEntity = Diciplina.class)
    private List<Diciplina> diciplinas;

    @ManyToOne
    private Serie serie;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull
    private String nome;

    @NotNull
    private int n_aulas;

    @NotNull
    private Date data_inicio;
    @NotNull

    private Timestamp data_controle;
    
    private boolean ativo;

}
