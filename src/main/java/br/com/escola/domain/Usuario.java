package br.com.escola.domain;
import lombok.Data;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;

@Entity
public @Data class Usuario {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull
    private String email;

    @NotNull
    private String password;

    @NotNull
    private String nome_completo;

    @LastModifiedDate
    private Timestamp data_controle;

    private boolean ativo;

}
