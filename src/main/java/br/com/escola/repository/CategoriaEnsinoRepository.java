package br.com.escola.repository;

import br.com.escola.domain.CategoriaEnsino;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoriaEnsinoRepository extends JpaRepository <CategoriaEnsino, Integer> {
}
