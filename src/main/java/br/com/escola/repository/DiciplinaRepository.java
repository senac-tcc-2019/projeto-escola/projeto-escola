package br.com.escola.repository;

import br.com.escola.domain.Diciplina;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DiciplinaRepository extends JpaRepository<Diciplina,Integer> {
}
