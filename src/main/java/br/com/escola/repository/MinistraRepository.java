package br.com.escola.repository;

import br.com.escola.domain.Ministra;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MinistraRepository extends JpaRepository<Ministra,Integer> {
}
