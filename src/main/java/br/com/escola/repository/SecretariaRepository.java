package br.com.escola.repository;

import br.com.escola.domain.Secretaria;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SecretariaRepository extends JpaRepository <Secretaria,Integer> {

}
