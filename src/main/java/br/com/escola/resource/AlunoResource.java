package br.com.escola.resource;

import br.com.escola.domain.Aluno;
import br.com.escola.domain.Usuario;
import br.com.escola.repository.AlunoRepository;
import br.com.escola.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/aluno")
public class AlunoResource {

    @Autowired
    AlunoRepository alunoRepository;

    @GetMapping
    public List<Aluno> ListAll(){return alunoRepository.findAll();}

    @GetMapping("/{id}")
    public Aluno findOne (@PathVariable int id){return alunoRepository.findById(id).get();}

    @PostMapping
    public Aluno save(@RequestBody Aluno aluno){return alunoRepository.save(aluno);}

    @PutMapping
    public ResponseEntity<Aluno> editar(@RequestBody Aluno aluno){
        Aluno alu = alunoRepository.save(aluno);
        return new ResponseEntity<Aluno>(alu, HttpStatus.OK);}

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id){
        Aluno aluno= alunoRepository.findById(id).get();
        alunoRepository.delete(aluno);
    }


}
