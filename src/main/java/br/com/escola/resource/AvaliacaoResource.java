package br.com.escola.resource;


import br.com.escola.domain.Avaliacao;
import br.com.escola.repository.AvaliacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/avaliacao")
public class AvaliacaoResource {

    @Autowired
    AvaliacaoRepository avaliacaoRepository;

    @GetMapping
    public List<Avaliacao> ListAll(){return avaliacaoRepository.findAll();}

    @GetMapping("/{id}")
    public Avaliacao findOne (@PathVariable int id){return avaliacaoRepository.findById(id).get();}

    @PostMapping
    public Avaliacao save(@RequestBody Avaliacao avaliacao){return avaliacaoRepository.save(avaliacao);}

    @PutMapping
    public ResponseEntity<Avaliacao> editar(@RequestBody Avaliacao avaliacao){
        Avaliacao ava = avaliacaoRepository.save(avaliacao);
        return new ResponseEntity<Avaliacao>(ava, HttpStatus.OK);}

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id){
        Avaliacao avaliacao = avaliacaoRepository.findById(id).get();
        avaliacaoRepository.delete(avaliacao);
    }
}
