package br.com.escola.resource;

import br.com.escola.domain.CategoriaEnsino;
import br.com.escola.repository.CategoriaEnsinoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/categoriaensino")
public class CategoriaEnsinoResource {


    @Autowired
    CategoriaEnsinoRepository categoriaEnsinoRepository;

    @GetMapping
    public List<CategoriaEnsino> ListAll() {
        return categoriaEnsinoRepository.findAll();
    }

    @GetMapping("/{id}")
    public CategoriaEnsino findOne(@PathVariable int id) {
        return categoriaEnsinoRepository.findById(id).get();
    }

    @PostMapping
    public CategoriaEnsino save(@RequestBody CategoriaEnsino categoriaEnsino) {
        return categoriaEnsinoRepository.save(categoriaEnsino);
    }

    @PutMapping
    public ResponseEntity<CategoriaEnsino> editar(@RequestBody CategoriaEnsino categoriaEnsino) {
        CategoriaEnsino cat = categoriaEnsinoRepository.save(categoriaEnsino);
        return new ResponseEntity<CategoriaEnsino>(cat, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id){
       CategoriaEnsino categoriaEnsino = categoriaEnsinoRepository.findById(id).get();
        categoriaEnsinoRepository.delete(categoriaEnsino);
    }
}
