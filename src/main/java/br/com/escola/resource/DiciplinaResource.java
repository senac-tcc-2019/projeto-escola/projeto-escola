package br.com.escola.resource;

import br.com.escola.domain.Diciplina;
import br.com.escola.repository.DiciplinaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/diciplina")
public class DiciplinaResource {

    @Autowired
    DiciplinaRepository diciplinaRepository;

    @GetMapping
    public List<Diciplina> ListAll(){return diciplinaRepository.findAll();}

    @GetMapping("/{id}")
    public Diciplina findOne (@PathVariable int id){return diciplinaRepository.findById(id).get();}

    @PostMapping
    public Diciplina save(@RequestBody Diciplina diciplina){return diciplinaRepository.save(diciplina);}

    @PutMapping
    public ResponseEntity<Diciplina> editar(@RequestBody Diciplina diciplina){
        Diciplina di = diciplinaRepository.save(diciplina);
        return new ResponseEntity<Diciplina>(di, HttpStatus.OK);}

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id){
        Diciplina diciplina = diciplinaRepository.findById(id).get();
        diciplinaRepository.delete(diciplina);
    }
}
