package br.com.escola.resource;

import br.com.escola.domain.Frequencia;
import br.com.escola.domain.Usuario;
import br.com.escola.repository.FrequenciaRepository;
import br.com.escola.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/frequencia")
public class FrequenciaResource {

    @Autowired
    FrequenciaRepository frequenciaRepository;

    @GetMapping
    public List<Frequencia> ListAll(){return frequenciaRepository.findAll();}

    @GetMapping("/{id}")
    public Frequencia findOne (@PathVariable int id){return frequenciaRepository.findById(id).get();}

    @PostMapping
    public Frequencia save(@RequestBody Frequencia frequencia){return frequenciaRepository.save(frequencia);}

    @PutMapping
    public ResponseEntity<Frequencia> editar(@RequestBody Frequencia frequencia){
        Frequencia fre = frequenciaRepository.save(frequencia);
        return new ResponseEntity<Frequencia>(fre, HttpStatus.OK);}

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id){
        Frequencia frequencia = frequenciaRepository.findById(id).get();
        frequenciaRepository.delete(frequencia);
    }
}
