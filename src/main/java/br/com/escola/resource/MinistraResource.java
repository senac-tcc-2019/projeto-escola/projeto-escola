package br.com.escola.resource;

import br.com.escola.domain.Ministra;
import br.com.escola.repository.MinistraRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;


@RestController
@RequestMapping("/ministra")
public class MinistraResource {

    @Autowired
    MinistraRepository ministraRepository;


    @GetMapping
    public List<Ministra> ListAll() {
        return ministraRepository.findAll();
    }

    @GetMapping("/{id}")
    public Ministra findOne(@PathVariable int id) {
        return ministraRepository.findById(id).get();
    }

    @PostMapping
    public Ministra save(@RequestBody Ministra ministra) {
        return ministraRepository.save(ministra);
    }

    @PutMapping
    public ResponseEntity<Ministra> editar(@RequestBody Ministra ministra) {
        Ministra minis = ministraRepository.save(ministra);
        return new ResponseEntity<Ministra>(minis, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        Ministra ministra = ministraRepository.findById(id).get();
        ministraRepository.delete(ministra);
    }
}
