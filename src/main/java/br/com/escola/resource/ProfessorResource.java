package br.com.escola.resource;

import br.com.escola.domain.Professor;
import br.com.escola.repository.ProfessorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/professor")
public class ProfessorResource {
    @Autowired
    ProfessorRepository professorRepository;

    @GetMapping
    public List<Professor> ListAll(){return professorRepository.findAll();}

    @GetMapping("/{id}")
    public Professor findOne (@PathVariable int id){return professorRepository.findById(id).get();}

    @PostMapping
    public Professor save(@RequestBody Professor professor){return professorRepository.save(professor);}

    @PutMapping
    public ResponseEntity<Professor> editar(@RequestBody Professor professor){
        Professor prof = professorRepository.save(professor);
        return new ResponseEntity<Professor>(prof, HttpStatus.OK);}

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id){
        Professor professor = professorRepository.findById(id).get();
        professorRepository.delete(professor);
    }
}
