package br.com.escola.resource;

import br.com.escola.domain.Secretaria;
import br.com.escola.repository.SecretariaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/secretaria")
public class SecretariaResource {

    @Autowired
    SecretariaRepository secretariaRepository;

    @GetMapping
    public List<Secretaria> ListAll() {
        return secretariaRepository.findAll();
    }

    @GetMapping("/{id}")
    public Secretaria findOne(@PathVariable int id) {
        return secretariaRepository.findById(id).get();
    }

    @PostMapping
    public Secretaria save(@RequestBody Secretaria secretaria) {
        return secretariaRepository.save(secretaria);
    }

    @PutMapping
    public ResponseEntity<Secretaria> editar(@RequestBody Secretaria secretaria) {
        Secretaria sec = secretariaRepository.save(secretaria);
        return new ResponseEntity<Secretaria>(sec, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id){
        Secretaria secretaria = secretariaRepository.findById(id).get();
        secretariaRepository.delete(secretaria);
    }


}
