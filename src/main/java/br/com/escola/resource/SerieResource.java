package br.com.escola.resource;

import br.com.escola.domain.CategoriaEnsino;
import br.com.escola.domain.Serie;
import br.com.escola.repository.CategoriaEnsinoRepository;
import br.com.escola.repository.SerieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/serie")
public class SerieResource {
    @Autowired
    SerieRepository serieRepository;

    @GetMapping
    public List<Serie> ListAll() {
        return serieRepository.findAll();
    }

    @GetMapping("/{id}")
    public Serie findOne(@PathVariable int id) {
        return serieRepository.findById(id).get();
    }

    @PostMapping
    public Serie save(@RequestBody Serie serie) {
        return serieRepository.save(serie);
    }

    @PutMapping
    public ResponseEntity<Serie> editar(@RequestBody Serie serie) {
        Serie ser = serieRepository.save(serie);
        return new ResponseEntity<Serie>(ser, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id){
        Serie serie = serieRepository.findById(id).get();
        serieRepository.delete(serie);
    }
}
