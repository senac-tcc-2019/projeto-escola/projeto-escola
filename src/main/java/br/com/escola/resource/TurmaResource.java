package br.com.escola.resource;

import br.com.escola.domain.Turma;
import br.com.escola.repository.TurmaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/turma")
public class TurmaResource {

    @Autowired
    TurmaRepository turmaRepository;

    @GetMapping
    public List<Turma> ListAll(){return turmaRepository.findAll();}

    @GetMapping("/{id}")
    public Turma findOne (@PathVariable int id){return turmaRepository.findById(id).get();}

    @PostMapping
    public Turma save(@RequestBody Turma turma){return turmaRepository.save(turma);}

    @PutMapping
    public ResponseEntity<Turma> editar(@RequestBody Turma turma){
        Turma tu = turmaRepository.save(turma);
        return new ResponseEntity<Turma>(tu, HttpStatus.OK);}

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id){
        Turma turma = turmaRepository.findById(id).get();
        turmaRepository.delete(turma);
    }
}
