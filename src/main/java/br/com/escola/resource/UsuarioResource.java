package br.com.escola.resource;

import br.com.escola.domain.Usuario;
import br.com.escola.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/usuario")
public class UsuarioResource {

    @Autowired
    UsuarioRepository usuarioRepository;

    @GetMapping
    public List<Usuario> ListAll(){return usuarioRepository.findAll();}

    @GetMapping("/{id}")
    public Usuario findOne (@PathVariable int id){return usuarioRepository.findById(id).get();}

    @PostMapping
    public Usuario save(@RequestBody Usuario usuario){return usuarioRepository.save(usuario);}

    @PutMapping
    public ResponseEntity<Usuario> editar(@RequestBody Usuario usuario){
        Usuario us = usuarioRepository.save(usuario);
        return new ResponseEntity<Usuario>(us, HttpStatus.OK);}

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id){
        Usuario usuario = usuarioRepository.findById(id).get();
        usuarioRepository.delete(usuario);
    }

    }



