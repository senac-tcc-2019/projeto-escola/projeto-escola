CREATE TABLE avaliacao(
  id INT AUTO_INCREMENT,
  descricao VARCHAR(45) NOT NULL,
  data DATE NOT NULL,
  diciplina_id INT NOT NULL,
  turma_id INT NOT NULL,
  professor_id INT NOT NULL,
  PRIMARY KEY (id),
    FOREIGN KEY (diciplina_id) REFERENCES diciplina(id),
    FOREIGN KEY (turma_id) REFERENCES turma(id),
    FOREIGN KEY (professor_id)REFERENCES professor(id))
ENGINE = InnoDB;