CREATE TABLE frequencia(
id INT AUTO_INCREMENT,
dia DATE NOT NULL,
presente CHAR NOT NULL,
aluno_id INT NOT NULL,
diciplina_id INT NOT NULL,
PRIMARY KEY (id),
FOREIGN KEY (aluno_id) references aluno(id),
FOREIGN KEY (diciplina_id) references diciplina(id)
)ENGINE = INNODB;