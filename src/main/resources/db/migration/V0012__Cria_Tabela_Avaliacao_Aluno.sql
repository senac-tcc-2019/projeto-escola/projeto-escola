CREATE TABLE aluno_avaliacao (
  id INT AUTO_INCREMENT,
  avaliacao_id INT NOT NULL,
  aluno_id INT NOT NULL,
  mencao DOUBLE NOT NULL,
  aprovado BOOLEAN NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (avaliacao_id)REFERENCES avaliacao(id),
    FOREIGN KEY (aluno_id)REFERENCES aluno(id)
)ENGINE = InnoDB;
