CREATE TABLE turma_diciplinas(
  id INT AUTO_INCREMENT,
  diciplinas_id INT NOT NULL,
  turma_id INT NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (diciplinas_id)REFERENCES diciplina(id),
  FOREIGN KEY (turma_id)REFERENCES turma(id)
  )ENGINE = InnoDB;