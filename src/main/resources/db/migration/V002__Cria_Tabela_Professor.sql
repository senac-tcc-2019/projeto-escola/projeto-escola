CREATE TABLE professor (
id INT AUTO_INCREMENT,
nome VARCHAR (50) NOT NULL,
cpf  VARCHAR (50) NOT NULL,
data_nascimento DATE,
ativo BOOLEAN,
usuario_id INT NOT NULL,
PRIMARY KEY (id),
FOREIGN KEY (usuario_id) REFERENCES usuario(id)
)ENGINE = INNODB;