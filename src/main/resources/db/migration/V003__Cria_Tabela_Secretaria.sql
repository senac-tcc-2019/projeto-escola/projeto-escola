CREATE TABLE secretaria (
id INT AUTO_INCREMENT,
nome VARCHAR (50) NOT NULL,
usuario_id INT NOT NULL,
PRIMARY KEY (id),
FOREIGN KEY (usuario_id) REFERENCES usuario(id)
)ENGINE = INNODB;