CREATE TABLE serie(
id INT AUTO_INCREMENT,
serie INT NOT NULL,
categoria_ensino_id INT NOT NULL,
PRIMARY KEY (id),
FOREIGN KEY (categoria_ensino_id) REFERENCES categoria_ensino(id)
)ENGINE = INNODB;