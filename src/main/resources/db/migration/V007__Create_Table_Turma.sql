CREATE TABLE turma(
  id INT AUTO_INCREMENT,
  nome VARCHAR(45) NOT NULL,
  n_aulas INT NOT NULL,
  data_inicio DATE NOT NULL,
  ativo BOOLEAN NOT NULL,
  data_controle TIMESTAMP  NOT NULL,
  serie_id INT NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (serie_id) REFERENCES serie(id)
  )
ENGINE = InnoDB;