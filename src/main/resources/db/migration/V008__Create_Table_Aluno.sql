CREATE TABLE aluno(
  id INT AUTO_INCREMENT,
  nome VARCHAR(45) NOT NULL,
  turma_id INT NOT NULL,
  cpf VARCHAR(45) NOT NULL,
  data_nascimento DATE NOT NULL,
  ativo BOOLEAN NOT NULL,
  PRIMARY KEY (id),
    FOREIGN KEY (turma_id) REFERENCES turma (id)
    )ENGINE = InnoDB;