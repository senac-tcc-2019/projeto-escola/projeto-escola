CREATE TABLE ministra (
  id INT AUTO_INCREMENT,
  diciplina_id INT NOT NULL,
  professor_id INT NOT NULL,
  turma_id INT NOT NULL,
  ano_letivo VARCHAR(45) NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (diciplina_id) REFERENCES diciplina(id),
    FOREIGN KEY (professor_id) REFERENCES professor(id),
    FOREIGN KEY (turma_id)REFERENCES turma(id))
ENGINE = InnoDB;